package com.devcamp.customervisitrestapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.customervisitrestapi.model.Visit;

@Service
public class VisitService {
    @Autowired
    private CustomerService customerService;

    Visit HcmToDanang = new Visit(1000000, 300000);
    Visit HcmToHaiPhong = new Visit(1000000, 300000);
    Visit HcmToHanoi = new Visit(1000000, 300000);
    Visit HcmToHoiAn = new Visit(1000000, 300000);

    public ArrayList<Visit> getAllVisit() {
        ArrayList<Visit> allVisit = new ArrayList<>();
        HcmToDanang.setCustomer(customerService.KH001);
        HcmToHaiPhong.setCustomer(customerService.KH002);
        HcmToHanoi.setCustomer(customerService.KH004);
        HcmToHoiAn.setCustomer(customerService.KH005);

        allVisit.add(HcmToDanang);
        allVisit.add(HcmToHaiPhong);
        allVisit.add(HcmToHanoi);
        allVisit.add(HcmToHoiAn);

        return allVisit;

    }

}
