package com.devcamp.customervisitrestapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customervisitrestapi.model.Customer;

@Service
public class CustomerService {

    Customer KH001 = new Customer("Tran Dai Nghia", true, "Gold");
    Customer KH002 = new Customer("Tran  Nghia", false, "Gold");
    Customer KH003 = new Customer("Tran Dai ", true, "Premium");
    Customer KH004 = new Customer(" Dai Nghia", false, "Diamond");
    Customer KH005 = new Customer("Tria", true, "Premium");

    public ArrayList<Customer> getAllCustomer() {
        ArrayList<Customer> allCustomer = new ArrayList<>();
        allCustomer.add(KH001);
        allCustomer.add(KH002);
        allCustomer.add(KH003);
        allCustomer.add(KH004);
        allCustomer.add(KH005);
        return allCustomer;

    }

}
