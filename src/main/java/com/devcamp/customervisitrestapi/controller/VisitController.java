package com.devcamp.customervisitrestapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customervisitrestapi.model.Visit;
import com.devcamp.customervisitrestapi.service.VisitService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class VisitController {
    @Autowired
    private VisitService visitService;

    @GetMapping("/visits")
    public ArrayList<Visit> getAllVisit() {
        ArrayList<Visit> allVisit = visitService.getAllVisit();
        return allVisit;

    }

}
