package com.devcamp.customervisitrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerVisitRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerVisitRestApiApplication.class, args);
	}

}
